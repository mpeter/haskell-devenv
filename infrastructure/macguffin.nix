# NixOS module defining macguffin server

{ config, lib, pkgs, ... } :
let
  cfg = config.services.macguffin;
  backend = import ../backend/default.nix {};
in {
  options.services.macguffin = {
    env = lib.mkOption {
      type = lib.types.string;
    };
    domain = lib.mkOption {
      type = lib.types.string;
    };
    debug = lib.mkOption {
      type = lib.types.bool;
      default = false;
    };
  };

  config = {
    # networking.extraHosts = "127.0.0.1 ${cfg.env}.${cfg.domain}";
    networking.firewall.allowedTCPPorts = [ 22 80 443 ];

    environment.systemPackages = [ pkgs.rsync ];

    users.extraUsers.macguffin = { group = "macguffin"; };
    users.extraGroups.macguffin = {};

    systemd.services.macguffin = {
      description = "macguffin backend";
      serviceConfig.User = "macguffin";
      wants = [ "network.target" "macguffin-create-db.service" ];
      after = [ "network.target" "macguffin-create-db.service" ];
      wantedBy = [ "multi-user.target" ];
      restartIfChanged = true;
      script = ''
        export PGDATABASE=macguffin
        export ENV=Production
        cd ${backend}
        ${backend}/bin/example-server
      '';
    };

    systemd.services.macguffin-create-db = {
      serviceConfig.Type = "oneshot";
      wants = [ "postgresql.service" ];
      after = [ "postgresql.service" ];
      restartIfChanged = true;
      script = ''
        ${config.services.postgresql.package}/bin/createuser macguffin | true
        ${config.services.postgresql.package}/bin/createdb macguffin -O macguffin | true
      '';
    };

    services.postgresql = {
      enable = true;
      dataDir = "/data/postgresql";
    };

    services.redis = {
      enable = true;
    };

    services.nginx = {
      enable = true;
      recommendedOptimisation = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;

      virtualHosts."${cfg.env}.${cfg.domain}" = {
        forceSSL = ! cfg.debug;
        enableACME = ! cfg.debug;
        locations."/" = {
          proxyPass = "http://127.0.0.1:8081";
        };
      };
    };
  };
}
