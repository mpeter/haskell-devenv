# Infrastructure definition

variable "do_token" {
  description = "Digital Ocean access token. See https://www.digitalocean.com/community/tutorials/how-to-use-the-digitalocean-api-v2#how-to-generate-a-personal-access-token"
}

variable "domain" {
  description = "The name of an existing Domain in your DigitalOcean account. Set it up here: https://cloud.digitalocean.com/networking/domains/"
}

variable "env" {
  description = "Name for the deployment environment, usually your name. This will be the subdomain for the server and appear in DigitalOcean resource names."
}

variable "do_region" {
  default = "nyc3"
}

provider "digitalocean" {
  token = "${var.do_token}"
}

resource "tls_private_key" "macguffin" {
  algorithm = "RSA"
}

resource "local_file" "private_key" {
  filename = "${path.module}/private_key"
  content  = "${tls_private_key.macguffin.private_key_pem}"

  provisioner "local-exec" {
    command = "chmod 600 ${path.module}/private_key"
  }
}

resource "digitalocean_ssh_key" "macguffin" {
  name       = "macguffin-#{var.env}"
  public_key = "${tls_private_key.macguffin.public_key_openssh}"
}

data "template_file" "host-nixos-config" {
  template = <<EOF
# NixOS module for macguffin host system
# Provisioned by Terraform

let
  macguffinModulePath = "/root/macguffin/infrastructure/macguffin.nix";
in
  { config, pkgs, lib, ... } :
    if lib.pathExists macguffinModulePath then
      {
        imports = [ macguffinModulePath ];

        services.macguffin.env = "${var.env}";
        services.macguffin.domain = "${var.domain}";
        # services.macguffin.debug = true;

        system.autoUpgrade.enable = true;
      }
    else
      lib.warn ("Macguffin NixOS module not found at " + macguffinModulePath + " . Skipping Macguffin configuration.") {}
EOF
}

resource "digitalocean_droplet" "macguffin" {
  name       = "macguffin-${var.env}"
  size       = "2gb"
  image      = "debian-9-x64"
  region     = "${var.do_region}"
  ssh_keys   = ["${digitalocean_ssh_key.macguffin.id}"]
  ipv6       = true

  private_networking = true

  connection {
    user        = "root"
    host        = "${digitalocean_droplet.macguffin.ipv4_address}"
    private_key = "${tls_private_key.macguffin.private_key_pem}"
  }

  provisioner "file" {
    content     = "${data.template_file.host-nixos-config.rendered}"
    destination = "/root/host.nix"
  }

  provisioner "remote-exec" {
    inline = [
      "echo -e 'silent\nshow-error\nretry=2' | tee ~/.curlrc > /dev/null",
      "wget 'https://raw.githubusercontent.com/elitak/nixos-infect/769dce60c939add8fae5bc9f03fc96d8661a76a7/nixos-infect' -O /root/nixos-infect",
      "echo 'Installing NixOS. Logging to /tmp/infect.log.'",
      "NIXOS_IMPORT=/root/host.nix NIX_CHANNEL=nixos-17.03 bash /root/nixos-infect 2>&1 > /tmp/infect.log",
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "echo 'NixOS version:'",
      "nixos-version",
      "echo 'Removing old system...'",
      "cp /old-root/root/host.nix /root/host.nix",
      "cp /old-root/root/.curlrc /root/.curlrc",
      "rm -rf /old-root",
      "echo Done."
    ]
  }
}

resource "digitalocean_floating_ip" "macguffin" {
  droplet_id = "${digitalocean_droplet.macguffin.id}"
  region     = "${digitalocean_droplet.macguffin.region}"
}

resource "digitalocean_record" "macguffin" {
  domain = "${var.domain}"
  type   = "A"
  name   = "${var.env}."
  value  = "${digitalocean_floating_ip.macguffin.ip_address}"
}


resource "null_resource" "deploy" {
  depends_on = [ "digitalocean_record.macguffin" ]

  triggers {
    droplet = "${digitalocean_droplet.macguffin.id}"
    always  = "${uuid()}"
  }

  connection {
    user        = "root"
    host        = "${digitalocean_floating_ip.macguffin.ip_address}"
    private_key = "${tls_private_key.macguffin.private_key_pem}"
  }

  provisioner "remote-exec" {
    inline = ["echo 'Hello, World!'"]
  }

  provisioner "file" {
    content     = "${data.template_file.host-nixos-config.rendered}"
    destination = "/root/host.nix"
  }

  provisioner "local-exec" {
    command = "./deploy -i ${path.module}/private_key root@${digitalocean_floating_ip.macguffin.ip_address}"
  }
}

output "ip" {
  value = "${digitalocean_floating_ip.macguffin.ip_address}"
}

output "fqdn" {
  value = "${digitalocean_record.macguffin.fqdn}"
}

output "private_key_file" {
  value = "${path.module}/private_key"
}
