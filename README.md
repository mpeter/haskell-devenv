# Haskell Web App

This project is a Haskell web-application. Specifically it implements a REST API for a Person and a Widget model, wwhich are persisted in PostgreSQL. There are also endpoints to use a counter in Redis. The application features a few tests which also use PostgreSQL but can be switched easily to in-memory SQLite as commented out in `AppSpec.hs`.

This Haskell application resides in `backend/`. The rest of this repo comprises a rich development environment featuring:

- pure, reproducible Nix builds
- deployment on NixOS
- infrastructure provisioning with Terraform
- automated development workflow with CI/CD on GitLab

This way, both tests and normal development happen in the exact same environment that is in production - down to the hash.

This environment depends only on Nix, the purely functional package manager: https://nixos.org/nix/

PostgreSQL, Redis, all Haskell libraries and any other dependencies are fully handled automatically by the nix shells used to enter test mode and development mode. Deploying is then one command away. SSHing into the production server to read the logs is then just one command away. If you do not want to get a surprise bill from your hosting provider, destroying the production server and all of its associated configuration is then just one command away.

There is a guide further down on how to customize this environment as it changes through development cycles and further dependencies are added.

### Install

[Install Nix](https://nixos.org/nix/download.html) and clone this Git repo.


### Build and Run

You need a Postgres instance, user, and database to run the app. Configure the database connection on the last line of [`./backend/src/Config.hs`](backend/src/Config.hs)

```
cd backend
nix-build
result/bin/example-server
```


### Develop and Test

You can run the tests or start a development server  in an impure environment using `stack`:

```
cd backend
nix-shell --run './test.sh'
nix-shell --run './dev.sh'
```


The development cycle can be improved by seting up `halive` auto-reloading development server.

The tests are impure because they require a running Postgresql instance. They can be run in the development shell.


### Provision and Deploy

The [staging environment](TODO) is automatically deployed from `master`. You can also deploy any branch or tag to staging with a click on GitLab CI.

If you want to test your changes separately, here's how to provision and deploy your own server:

```
cd infrastructure
nix-shell --run 'terraform plan'
# review plan
nix-shell --run 'terraform apply'
```

Terraform will ask you for a deployment environment name (eg. your name; don't use the names of existing deployment) and DigitalOcean security token. You can [save these variables](https://www.terraform.io/intro/getting-started/variables.html#assigning-variables). Do not commit them into the repo.

When it's done, it will output the IP and domain name of the server. SSH in to take a look around:

```
./ssh 'systemctl status macguffin'
```

You can run `terraform apply` again any time to deploy `git HEAD` to your server.

When you're done, destroy the infrastructure:

```
nix-shell --run 'terraform destroy'
```


### Provision a **staging** server and Contiuously Deploy `master`

Provision and deploy  a server as above, and name it `staging` by setting the value in the Terraform variable `env`.

On your GitLab project, go to **Settings** → **CI/CD** → **Secret variables**.
- set `SSH_PRIVATE_KEY` to the contents of the `private_key` file output by terraform
- set `SSH_HOST` to the DNS address of the staging server

Start a new pipeline for `master` on GitLab. It should build and deploy the app to the new staging server. The `master-deploy-staging` task will automatically run on push/merge to `master` and deploy the app. You can also deploy a specific branch/tag/commit to staging by starting the `deploy-staging` manual job on any pipeline.

On the GitLab repo, go to **CI/CD** → **Environments** to see the latest deployments. You can also set a URL for the staging environment, it will show up in the GitLab UI.

That's it. Be careful not to lose the Terraform state persisted in `infrastructure/terraform.tfstate`, otherwise you'll have to manually delete the server, volume, domain record etc on DO.


## Change the Setup

### Dependencies

- `./backend/haskell-app.cabal`
  - Haskell dependencies.
- [`./backend/default.nix`](backend/default.nix)
  - native dependencies of Haskell project
  - overrides for haskell packages
- [`./backend/shell.nix`](backend/shell.nix)
  - development shell with tools, loaded by `cd backend; nix-shell`

#### Updating Haskell Dependencies

After you change `haskell-app.cabal` or `stack.yaml`, you need to update the nix package definitions for the haskell dependencies:

```
cd backend
nix-shell --run update-deps
```

This will take a long time. It will produce `packages.nix`, `configuration-packages.nix`, `derivation.nix`. Now you can build with `nix-build`.


### OS Configuration and Services

The OS services, users, etc. are defined in the NixOS module [`./infrastructure/macguffin.nix`](infrastructure/macguffin.nix).

After changing it, deploy with `terraform` as above.

If the configuration is invalid, no changes are applied.

If you don't like the result, you can SSH in and instantly roll back with `nixos-rebuild switch --rollback`.

### Infrastructure

The infrastructure is defined in `infrastructure/main.tf`. After changing it, deploy with `terraform` as above.

There are no rollbacks, and changes are not atomic. If terraform fails, your system _might_ be in a degraded state (except when NixOS deployment fails, in which case there was no change). You must fix the configuration and apply again.

This is why you should change the infrastructure on a separate branch, and deploy a separate infrastructure environment to test it. It is also why eventually Terraform should also be run in CI to test before merge.


### Scaling and More Automation

When the infrastructure gets more complex (eg. adding load balancers, servers, domains, data storage, resources from multiple cloud providers), you should run Terraform in CI to continuously build, test and deploy your infrastructure, as well as show a plan of changes relative to the current master/staging deployments. You will need to configure [remote state](https://www.terraform.io/docs/state/remote.html).
