# Demo Haskell web API

## Build instructions

1. Set up a `Postgresql` database with two databases `test` and `main` each
	 owned by a user of the same name.  The database should be available at
	 `localhost:5432`.
2. Set up `Redis` with defaults. 
3. Run `stack test` to run tests.
4. Run `stack build` in the package directory.

## Demo location

When the program is running on the local machine, the documentation is
available at [http://localhost:8171/docs](http://localhost:8171/docs).
