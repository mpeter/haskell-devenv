{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE Trustworthy   #-}
{-# LANGUAGE TypeOperators #-}

{- Module: Api
 -
 - Specification of the API used to communicate with the microservice.
 -}
module Api (
  PersonAPI

  , PersonRecordAPI
  , CreatePerson
  , EditPerson
  , GetPersonById
  , GetPerson
  , DeletePersonById
  , GetCreatedAtById
  , GetUpdatedAtById
  , GetPersonId

  , CounterAPI
  , Counter
  , IncrementCounter
  , ResetCounter
  , Docs

  , WidgetAPI
  , ListWidgets
  , AddWidget
  , DeleteWidget
  , UsersWith
  , GetWidgetId
  ) where

import           Network.JSONApi                  (Document)
import           Servant.API
import           Servant.API.ContentTypes.JSONApi (JSONApi)
import           Servant.Swagger.UI               (SwaggerSchemaUI)
import           Types

-- API Endpoints related to working with Person records

type CreatePerson =
  "person" :> Header "requesting-user" UUID :>
  ReqBody '[JSON] CreateReq :> Post '[JSONApi] (Document Person)

type EditPerson =
  "person" :> Header "requesting-user" UUID :>
  ReqBody '[JSON] UpdateReq :> Put '[JSONApi] NoContent

type GetPersonById =
  "person" :> Capture "id" PersonId :> Get '[JSONApi] (Document Person)

type GetPerson  =
  "person" :> QueryParam "name" String :>
  QueryParam "ssn" String :> Get '[JSONApi] (Document Person)

type DeletePersonById =
  "person" :> Capture "id" PersonId :> Delete '[JSON] NoContent

type GetCreatedAtById =
  "person" :> Capture "id" PersonId :>
  "created_at" :> Get '[JSON] DateTime

type GetUpdatedAtById =
  "person" :> Capture "id" PersonId :>
  "updated_at" :> Get '[JSON] DateTime

type GetPersonId = "person" :>
  Capture "uuid" UUID :>
  "id" :>
  Get '[JSON] [PersonId]

--
-- API endpoints related to the counter functionality
--

type Counter = "counter" :> Get '[JSON] Int

type IncrementCounter = "counter" :> "increment" :>
  Header "requesting-user" UUID :> Get '[JSON] NoContent

type ResetCounter = "counter" :> "reset" :>
  Header "requesting-user" UUID :> Get '[JSON] NoContent

--
-- API endpoints related to widgets
--

type ListWidgets = "person" :>
  Capture "id" PersonId :>
  "widgets" :> Get '[JSONApi] (Document Widget)

type AddWidget = "person" :>
  Capture "id" PersonId :>
  "widgets" :>
  Header "requesting-user" UUID :>
  ReqBody '[JSON] Widget :> Post '[JSON] NoContent

type DeleteWidget = "person" :>
  Capture "id" PersonId :>
  "widgets" :>
  Capture "wid" WidgetId :>
  Header "requesting-user" UUID :>
  Delete '[JSON] NoContent

type UsersWith = "widget" :>
  Capture "name" Text :>
  "users" :>
  Get '[JSONApi] (Document Person)

type GetWidgetId = "widget" :>
  Capture "name" String :>
  Capture "color" String :>
  "id" :>
  Get '[JSON] [WidgetId]

-- | Endpoint for automatic documentation
type Docs = SwaggerSchemaUI "docs" "swagger.json"

-- API for working with Person records
type PersonRecordAPI  =
  CreatePerson
  :<|> EditPerson
  :<|> DeletePersonById
  :<|> GetPersonById
  :<|> GetPerson
  :<|> GetCreatedAtById
  :<|> GetUpdatedAtById
  :<|> GetPersonId

-- Counter API specification
type CounterAPI =
  Counter
  :<|> IncrementCounter
  :<|> ResetCounter

-- Widget API specification
type WidgetAPI =
  ListWidgets
  :<|> AddWidget
  :<|> DeleteWidget
  :<|> UsersWith
  :<|> GetWidgetId

type PersonAPI = PersonRecordAPI :<|> CounterAPI :<|> WidgetAPI
