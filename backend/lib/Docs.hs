{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Trustworthy       #-}

module Docs (swaggerDoc) where

import           Api
import           Control.Lens
import           Data.Proxy
import           Data.Swagger
import           Data.Swagger.JSONApi ()
import           Servant.Swagger

-- | The swagger documentation for the API
swaggerDoc :: Swagger
swaggerDoc = toSwagger (Proxy :: Proxy PersonAPI)
 -- General information
 & info.title .~ "Person API"
 & info.version .~ "1.0"
 & info.description .~ Just "Programmatically access our database of people"
 & info.license ?~ "© PM"
 & host ?~ "51.15.215.176:8171"
 -- Summaries
 & createPerson.summary ?~ "Create a new record"
 & editPerson.summary ?~ "Edit an existing record"
 & deletePerson.summary ?~ "Delete a record"
 & getPerson . summary ?~ "Get person using their name or SSN"

-- Lenses for API components

-- Helpers
papi :: Proxy PersonAPI
papi = Proxy

createPerson :: Traversal' Swagger Operation
createPerson = subOperations (Proxy :: Proxy CreatePerson) papi

getPerson :: Traversal' Swagger Operation
getPerson = subOperations (Proxy :: Proxy GetPerson) papi

editPerson :: Traversal' Swagger Operation
editPerson = subOperations (Proxy :: Proxy EditPerson) papi

deletePerson :: Traversal' Swagger Operation
deletePerson = subOperations (Proxy :: Proxy DeletePersonById) papi
