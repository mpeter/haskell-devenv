{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

-- |
-- Module: Types.TH
--
-- Template Haskell for generating types
module Types.TH (
  Person (..)
  , PersonId
  , Widget (..)
  , WidgetId
  , PersonWidget (..)
  , EntityField (..)
  , Unique (..)
  , Key (..)
  , migrateAll ) where

import           Database.Persist.Class (EntityField (..), Key (..),
                                         Unique (..))
import           Database.Persist.Quasi (lowerCaseSettings)
import           Database.Persist.TH    (mkMigrate, mkPersist, persistFileWith,
                                         share, sqlSettings)
import           GHC.Generics
import           Types.Misc

-- | Person is the fundamental type for this app
-- Note that I used a splice rather than a quasiquoter so that I could leave
-- the type declarations in their own file.
share [mkPersist sqlSettings, mkMigrate "migrateAll"] $(persistFileWith lowerCaseSettings "lib/Entities.persist")
