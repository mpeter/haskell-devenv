{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedLists            #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TemplateHaskell            #-}

-- |
-- Module: Types.Person
--
-- The person type
module Types.Person (
  Person (..)
  , PersonId
  , Unique (..)
  , Key (..)
  , EntityField (..)
  , module Types.Person
  ) where

import           Control.Lens    (makeLensesFor, view, (&), (.~), (?~))
import qualified Data.Aeson.TH   as ATH
import           Data.Int        (Int64)
import           Data.Proxy
import           Data.Swagger
import qualified Data.Text       as T
import           Data.Time       (diffDays)
import           GHC.Generics
import           Network.JSONApi (ResourcefulEntity (..))
import           Types.CreateReq (CreateReq (CR))
import           Types.Misc
import           Types.TH
import           Types.UpdateReq (UpdateReq (UR))

-- This function allows us to choose the name
makeLensesFor
  [ ("personUpdatedBy","updatedBy")
  , ("personUpdatedAt", "updatedAt")
  , ("personUniqueID","pUniqueIdL")]
  ''Person

-- Custom ToJSON, FromJSON instances for Person
ATH.deriveJSON ATH.defaultOptions { ATH.fieldLabelModifier = modifier 6 } $ ''Person

-- ToSchema is used to generate Swagger docs
instance ToSchema Person where
  declareNamedSchema _ = do
    uuidS <- declareSchemaRef (Proxy :: Proxy UUID)
    textS <- declareSchemaRef (Proxy :: Proxy Text)
    dateS <- declareSchemaRef (Proxy :: Proxy Date)
    dateTimeS <- declareSchemaRef (Proxy :: Proxy DateTime)
    boolS <- declareSchemaRef (Proxy :: Proxy Bool)
    return $ NamedSchema (Just "Person") $ mempty
      & type_ .~ SwaggerObject
      & properties .~
        [ ("uniqueID", uuidS)
        , ("lastName", textS)
        , ("middleName", textS)
        , ("firstName", textS)
        , ("suffix", textS)
        , ("ssn", textS)
        , ("dateOfBirth", dateS)
        , ("isDeceased", boolS)
        , ("dateOfDeath", dateS)
        , ("createdAt", dateTimeS)
        , ("updatedAt", dateTimeS)
        , ("createdBy", uuidS)
        , ("updatedBy", uuidS) ]
      & required .~
        [ "uniqueID"
        , "lastName"
        , "middleName"
        , "firstName"
        , "suffix"
        , "ssn"
        , "dateOfBirth"
        , "isDeceased"
        , "createdAt"
        , "createdBy"]

-- This is used to simplify access to the uniqueId
instance HasUniqueId Person where
  uniqueIdL = pUniqueIdL

-- This instance makes it possible to marshal a Person into a JSON API Document
instance ResourcefulEntity Person where
  resourceIdentifier = T.pack . show . view uniqueIdL
  resourceType _ = "Person"
  resourceLinks _ = Nothing
  resourceMetaData _ = Nothing
  resourceRelationships _ = Nothing

deriving instance Generic PersonId
deriving instance Enum PersonId
deriving instance Num PersonId
deriving instance Real PersonId

-- Integral instance needed for comparison with ints
deriving instance Integral PersonId

-- For Swagger
instance ToSchema PersonId where
  declareNamedSchema _ = do
    int64Schema <- declareSchema (Proxy :: Proxy Int64)
    return $ NamedSchema (Just "PersonId") $ int64Schema

-- For Swagger
instance ToParamSchema PersonId where
  toParamSchema _ = mempty
    & type_ .~ SwaggerInteger

--
-- Helper functions
--

-- | Assemble a person from a 'CreateReq', the UUIDs of the creator and the new
-- user and the date, performing some validation.
newPerson :: CreateReq -> UUID -> UUID -> DateTime -> Either String Person
newPerson (CR fn mn ln sx sn dob dec dod) newUuid creator dt = case dod of
  Nothing -> Right p
  Just d  -> if diffDays d dob >= 0 && diffDays d dob < 125 then Right p else Left "Invalid lifespan."
  where
    p = Person newUuid ln mn fn sx sn dob dec dod dt Nothing creator Nothing

-- | Update a Person
updatePerson :: Person -> UpdateReq -> UUID -> DateTime -> Person
updatePerson p req updater d = p
  & updateReq .~ req
  & updatedBy ?~ updater
  & updatedAt ?~ d

-- A Lens from a Person into an UpdateReq
updateReq :: Functor f => (UpdateReq -> f UpdateReq) -> Person -> f Person
updateReq q (Person uid ln mn fn sx sn dob dec dod ca ua cb ub) =
  fmap g $ q $ UR uid fn mn ln sx sn dob dec dod
  where
    g (UR _ fn mn ln sx sn dob dec dod)
      = Person uid ln mn fn sx sn dob dec dod ca ua cb ub
