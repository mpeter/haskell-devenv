{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedLists            #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TemplateHaskell            #-}

-- |
-- Module: Types.Widget
--
-- The widget type
module Types.Widget (
  Widget (..)
  , WidgetId
  , PersonWidget (..)
  , Unique (..)
  , Key (..)
  , EntityField (..)
  , module Types.Widget
  ) where

import           Control.Lens    ((&), (.~), (?~))
import qualified Data.Aeson.TH   as ATH
import           Data.Int        (Int64)
import           Data.Proxy
import           Data.Swagger
import qualified Data.Text       as T
import           GHC.Generics
import           Network.JSONApi (ResourcefulEntity (..))
import           Types.Misc
import           Types.TH

-- Custom ToJSON, FromJSON instance for Widget
ATH.deriveJSON ATH.defaultOptions { ATH.fieldLabelModifier = modifier 6 } $ ''Widget

-- Used to generate the Swagger docs
instance ToSchema Widget where
  declareNamedSchema _ = do
    intS   <- declareSchemaRef (Proxy :: Proxy Int)
    textS  <- declareSchemaRef (Proxy :: Proxy Text)
    colorS <- declareSchemaRef (Proxy :: Proxy Color)
    return $ NamedSchema (Just "Widget") $ mempty
      & type_ .~ SwaggerObject
      & properties .~
        [ ("color", colorS)
        , ("name", textS) ]
      & required .~ ["color", "name"]

-- Used to marshal a Widget into a JSON API document
instance ResourcefulEntity Widget where
  resourceIdentifier = T.pack . show . widgetColor
  resourceType _ = "Widget"
  resourceLinks _ = Nothing
  resourceMetaData _ = Nothing
  resourceRelationships _ = Nothing

deriving instance Generic WidgetId
deriving instance Enum WidgetId
deriving instance Num WidgetId
deriving instance Real WidgetId

-- Integral instance needed for comparison with ints
deriving instance Integral WidgetId

-- Swagger
instance ToSchema WidgetId where
  declareNamedSchema _ = do
    int64Schema <- declareSchema (Proxy :: Proxy Int64)
    return $ NamedSchema (Just "WidgetId") $ int64Schema

-- Swagger
instance ToParamSchema WidgetId where
  toParamSchema _ = mempty
    & type_ .~ SwaggerInteger
