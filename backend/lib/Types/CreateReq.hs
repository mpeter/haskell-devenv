{-# LANGUAGE DeriveGeneric #-}
module Types.CreateReq (
  CreateReq(..)
  ) where

import           Data.Aeson
import           Data.Swagger
import           GHC.Generics
import           Types.Misc

--
-- CreatePersonRequest definition and instances
--

data CreateReq =
  CR
  { firstName   :: Text
  , middleName  :: Text
  , lastName    :: Text
  , suffix      :: Text
  , ssn         :: Text
  , dateOfBirth :: Date
  , isDeceased  :: Bool
  , dateOfDeath :: (Maybe Date) }
  deriving Generic

instance ToJSON CreateReq
instance FromJSON CreateReq
instance ToSchema CreateReq
