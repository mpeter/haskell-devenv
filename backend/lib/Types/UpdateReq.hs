{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE TemplateHaskell #-}
module Types.UpdateReq (
  UpdateReq(..)
  ) where

import           Control.Lens
import           Data.Aeson
import           Data.Swagger
import           GHC.Generics
import           Types.Misc

--
-- UpdatePersonRequest definition and instances
--

data UpdateReq =
  UR
  { uniqueId    :: UUID
  , firstName   :: Text
  , middleName  :: Text
  , lastName    :: Text
  , suffix      :: Text
  , ssn         :: Text
  , dateOfBirth :: Date
  , isDeceased  :: Bool
  , dateOfDeath :: (Maybe Date) }
  deriving Generic

makeLensesFor [("uniqueId","urUniqueIdL")] ''UpdateReq

instance HasUniqueId UpdateReq where
  uniqueIdL = urUniqueIdL

instance ToJSON UpdateReq
instance FromJSON UpdateReq
instance ToSchema UpdateReq
