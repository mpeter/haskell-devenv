{ pkgs ? import ../pinned-nixpkgs.nix {}, ... } :
((import ./derivation.nix { nixpkgs = pkgs; }).override {
  overrides = self: super: {
    Cabal = pkgs.haskellPackages.Cabal;
    haskeline = pkgs.haskellPackages.haskeline;
    xhtml = pkgs.haskellPackages.xhtml;
    halive = pkgs.haskellPackages.halive;
  };
}).haskell-app
