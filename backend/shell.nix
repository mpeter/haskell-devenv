# Nix shell environment for backend development

{ pkgs ? import ../pinned-nixpkgs.nix {}, ... }:
let

  stackage2nix = let
    src = pkgs.fetchFromGitHub {
      owner = "typeable";
      repo = "stackage2nix";
      rev = "b19fea5eff5846dda613b22f373a0def0b33b6fb";
      sha256 = "0yqgddicnmiwqs4pnf0glzp9lgh5xry87q82chkmcngn2vxc0h8h";
    };
    haskellPackages = import (src + /nix/haskellPackages) { nixpkgs = pkgs; };
  in pkgs.stdenv.mkDerivation rec {
    name = "stackage2nix-${version}";
    version = haskellPackages.stackage2nix.version;
    phases = [ "installPhase" "fixupPhase" ];
    buildInputs = [ pkgs.makeWrapper ];

    installPhase = ''
      mkdir -p $out/bin
      makeWrapper ${haskellPackages.stackage2nix}/bin/stackage2nix $out/bin/stackage2nix \
        --prefix PATH ":" "${pkgs.nix}/bin" \
        --prefix PATH ":" "${pkgs.nix-prefetch-scripts}/bin"
    '';
  };
  update-deps = pkgs.writeScriptBin "update-deps" ''
    #!${pkgs.stdenv.shell}
    set -e

    rm -rf ../stackage2nix-stuff
    mkdir -p ../stackage2nix-stuff

    echo; echo "Downloading Hackage Index..."
    mkdir -p ../stackage2nix-stuff/hackage-index/
    pushd ../stackage2nix-stuff/hackage-index/
    ${pkgs.curlFull}/bin/curl -L -O --retry 5 -o 01-index.tar.gz https://hackage.haskell.org/01-index.tar.gz
    ${pkgs.gzip}/bin/gunzip --keep 01-index.tar.gz
    popd

    echo; echo "Downloading all-cabal-hashes..."
    mkdir -p ../stackage2nix-stuff/all-cabal-hashes/
    ${pkgs.git}/bin/git clone --branch hackage https://github.com/commercialhaskell/all-cabal-hashes.git ../stackage2nix-stuff/all-cabal-hashes/

    echo; echo "Downloading lts-haskell..."
    mkdir -p ../stackage2nix-stuff/lts-haskell/
    ${pkgs.git}/bin/git clone --depth 1 https://github.com/fpco/lts-haskell.git ../stackage2nix-stuff/lts-haskell/

    echo; echo "Running stackage2nix..."
    ${stackage2nix}/bin/stackage2nix \
      --lts-haskell ../stackage2nix-stuff/lts-haskell/ \
      --hackage-db ../stackage2nix-stuff/hackage-index/01-index.tar \
      --all-cabal-hashes ../stackage2nix-stuff/all-cabal-hashes/ \
      --out-derivation derivation.nix \
      --no-check \
      stack.yaml

      echo; echo "Done updating nix packages for haskell dependencies."
  '';
  postgresql = pkgs.postgresql95;
  ephemeralpg = pkgs.ephemeralpg.override (old : {
    inherit postgresql;
  });
in pkgs.lib.overrideDerivation (import ./default.nix { inherit pkgs; }) (old : {
  name = "macguffin-backend-shell";
  buildInputs = old.buildInputs ++ [ update-deps stackage2nix postgresql ephemeralpg pkgs.stack pkgs.nix pkgs.stdenv pkgs.haskell.packages.ghc802.ghc pkgs.redis ];
})
