#!/bin/sh
set -e

echo "Building...."
stack build

echo "Starting redis..."
redis-server &
REDIS_PID=$!

echo "Creating development database..."
export PGCONN="$(pg_tmp -w 3600 -d "$(pwd)/testdb" start)"
echo "Database URL: " "$PGCONN"

set +e
echo "Running 'stack test haskell-app'..."
stack test haskell-app
RESULT=$?

echo "Stopping test database..."
pg_tmp  -d "$(pwd)/testdb" stop

echo "Stopping redis..."
kill $REDIS_PID
sleep 1

[[ "$RESULT" == "0" ]] && exit 0 || exit 1
