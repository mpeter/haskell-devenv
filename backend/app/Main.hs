{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings         #-}

module Main where

import           Control.Monad.IO.Class      (liftIO)
import           Control.Monad.Logger        (runStdoutLoggingT)
import           Control.Monad.Reader        (runReaderT)
import           Database.Persist.Postgresql (SqlBackend, withPostgresqlPool)
import           Database.Persist.Sql        (runSqlPool)
import           Database.Redis              (defaultConnectInfo, runRedis)
import qualified Database.Redis              as R
import           Network.Wai.Handler.Warp    (run)
import           Run.Db                      (RunDb, connString, initDb)
import           Run.Servers                 (mkApp)

main :: IO ()
main = do
  cs <- connString
  initRedis
  runStdoutLoggingT $
    withPostgresqlPool cs 1 $ \x -> do
      let x' = flip runSqlPool x :: RunDb SqlBackend
      initDb x'
      liftIO $ run 8171 $ mkApp x'


-- Script to set the initial value of the counter
initRedis :: IO ()
initRedis = do
  conn <- R.connect defaultConnectInfo
  runRedis conn $ do
    R.set "counter" "0"
    return ()
