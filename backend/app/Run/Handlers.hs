{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE Trustworthy                #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}

-- |
-- Module: Run.Handlers
--
-- Logic for handling API requests.
module Run.Handlers (
  -- * Person
    createPerson
  , editPerson
  , deletePerson
  , getPersonById
  , getPerson
  , getCreatedAtById
  , getUpdatedAtById
  , getPersonId

  -- * Counter
  , counter
  , incrementCounter
  , resetCounter

  -- * Widget
  , listWidgets
  , addWidget
  , deleteWidget
  , usersWith
  , getWidgetId
) where

import           Control.Lens               (view)
import           Control.Monad              (unless)
import           Control.Monad.Except
import           Control.Monad.Logger       (LogLevel (..), MonadLogger (..),
                                             defaultLoc)
import           Control.Monad.Trans.Reader
import qualified Data.Aeson                 as AE
import           Data.ByteString.Lazy       (fromStrict, toStrict)
import           Data.Either                (either)
import           Data.Text                  (Text)
import           Data.Time                  (getCurrentTime)
import qualified Data.UUID                  as UUID
import qualified Database.Esqueleto         as E
import           Database.Persist
import           Database.Persist.Sqlite
import           Database.Redis             (MonadRedis (..))
import qualified Database.Redis             as R
import           Network.JSONApi            (Document, mkDocument)
import           Run.Error                  (AppError (..))
import           Run.Validation             (parseName, validateSsn)
import           Servant.API                (NoContent (..))
import           System.Random
import           Types                      hiding (Text)
import qualified Types

--
-- | Add a 'Person' to the database
createPerson :: (MonadError AppError m, MonadLogger m, MonadIO m) =>
  Maybe UUID ->
  CreateReq ->
  ReaderT SqlBackend m (Document Person)
createPerson creator req = do
  logInfo "createPerson" "Creating new person."
  uuid <- UUID.toString <$> (liftIO $ randomIO)
  ca   <- liftIO $ getCurrentTime
  cb   <- maybe (throwError $ MissingHeader "requesting-user") return creator
  p    <- either (throwError . AppError) return $ newPerson req uuid cb ca
  k    <- insert p
  logInfo "createPerson" $ "New key = " ++ show k
  return $ mkDocument [p] Nothing Nothing

--
-- | Edit a 'Person' in the database (by replacement)
editPerson :: (MonadIO m, MonadError AppError m, MonadLogger m) =>
  Maybe UUID ->
  UpdateReq ->
  ReaderT SqlBackend m NoContent
editPerson updater req = do
  logInfo "editPerson" "Editing person"
  let uuid = UniqueID $ view uniqueIdL req
  ua <- liftIO $ getCurrentTime
  ub <- maybe (throwError $ MissingHeader "requesting-user") return updater
  res <- getBy uuid
  p <- maybe (throwError $ NoResult) return res
  replace (entityKey p) (updatePerson (entityVal p) req ub ua)
  return NoContent

--
-- | Delete a 'Person' from the database
deletePerson :: (MonadIO m, MonadLogger m) =>
  PersonId ->
  ReaderT SqlBackend m NoContent
deletePerson pid = do
  logInfo "deletePerson" "Deleting person"
  delete $ pid
  return NoContent

--
-- | Get a 'Person' by their 'PersonId'
getPersonById :: (MonadIO m, MonadError AppError m, MonadLogger m) =>
  PersonId ->
  ReaderT SqlBackend m (Document Person)
getPersonById i = do
  logInfo "getPersonById" "Getting person by id"
  let
    err = do
      logError "getPesonById" "No results"
      throwError NoResult
  p <- get i >>= maybe err return
  return $ mkDocument [p] Nothing Nothing

--
-- Get a person using their name
-- PERSIST
getPersonByName ::
  (MonadIO m, MonadError AppError m) =>
  String ->
  ReaderT SqlBackend m [Person]
getPersonByName name = do
  (fn, ln) <- either (throwError . AppError) return $ parseName name
  fmap entityVal <$> selectList [PersonFirstName ==. fn, PersonLastName ==. ln] []

--
-- Get a person using their name
-- ESQUELETO
getPersonByName' :: (MonadIO m, MonadError AppError m) =>
  String ->
  ReaderT SqlBackend m [Person]
getPersonByName' name = do
    (fn, ln) <- either (throwError . AppError) return $ parseName name
    res <- E.select $
      E.from $ \p -> do
        E.where_ (p E.^. PersonFirstName E.==. E.val fn)
        E.where_ (p E.^. PersonLastName E.==. E.val ln)
        return p
    return $ fmap entityVal res

invalidSsn :: AppError
invalidSsn = AppError "Invalid ssn.  Use 'xxx-xx-xxxx'."

--
-- Get a 'Person' using their SSN
-- PERSIST
getPersonBySSN :: (MonadIO m, MonadError AppError m) =>
  String ->
  ReaderT SqlBackend m [Person]
getPersonBySSN s = do
  unless (validateSsn s) $ throwError invalidSsn
  fmap entityVal <$> selectList [PersonSsn ==. s] []

-- Get a person using their SSN
-- ESQUELETO
getPersonBySSN' :: (MonadIO m, MonadError AppError m) =>
  String ->
  ReaderT SqlBackend m [Person]
getPersonBySSN' s = do
  unless (validateSsn s) $ throwError invalidSsn
  res <- E.select $ -- Evaluates an expression into a monadic context
    -- the table to use is inferred by the type and `from` takes a filtering
    -- function on the appropriate values
    E.from $ \p -> do
      E.where_ (p E.^. PersonSsn E.==. E.val s) -- PersonSsn is a data constructor for EntityField Person String
      -- Might short circuit before we get to this instruction
      return p
  return $ fmap entityVal res

--
-- | Get a person using either their name or ssn
getPerson :: (MonadIO m, MonadLogger m, MonadError AppError m) =>
  Maybe String -> -- ^ name: Last, First
  Maybe String -> -- ^ SSN
  ReaderT SqlBackend m (Document Person)
getPerson n s = do
  logInfo "getPerson" $ "Getting person with name = " ++ show n ++ " or ssn = " ++ show s
  ps <- case n of
    Just n' -> getPersonByName' n'
    Nothing ->
      maybe (throwError $ AppError "No parameters given!")
        getPersonBySSN' s
  return $ mkDocument ps Nothing Nothing

--
-- | Get creation timestamp by 'PersonId'
getCreatedAtById :: (MonadLogger m, MonadIO m, MonadError AppError m) =>
  PersonId ->
  ReaderT SqlBackend m DateTime
getCreatedAtById pid = do
  logInfo "getCreatedAtById" "Fetching creation timestamp"
  res <- get pid
  maybe (throwError $ NoResult) (return . personCreatedAt) res

--
-- | Get update timestamp by 'PersonId'
getUpdatedAtById :: (MonadError AppError m, MonadIO m, MonadLogger m) =>
  PersonId ->
  ReaderT SqlBackend m DateTime
getUpdatedAtById pid = do
  logInfo "getUpdatedAtById" "Fetching the most recent update timestamp"
  res <- get pid
  maybe (throwError $ NoResult) (return . personUpdatedAt) res >>=
    maybe (throwError $ AppError "Never updated.") return
--
-- | Get the (list of) id(s) that correspond to a person
getPersonId :: (MonadError AppError m, MonadLogger m, MonadIO m) =>
  UUID ->
  ReaderT SqlBackend m [PersonId]
getPersonId uid = do
  logInfo "getPersonId" $ "Looking up the info for " ++ show uid
  res <- selectList [PersonUniqueID ==. uid] []
  return $ fmap entityKey res

--
-- | View the current value of the counter
counter :: (MonadLogger m, MonadError AppError m, MonadRedis m) => m Int
counter = do
  logInfo "counter" "Getting counter"
  res <- liftRedis $ R.get "counter" -- We start with type Either Reply (Maybe ByteString); yikes!
  flip (either (throwError . AppError . show)) res $
    maybe (throwError NoResult) $ either (throwError . AppError) return . AE.eitherDecode . fromStrict


--
-- | Increment the counter
incrementCounter :: (MonadLogger m, MonadError AppError m, MonadRedis m) =>
  Maybe UUID -> -- ^ requesting-user header
  m NoContent
incrementCounter Nothing = throwError $ MissingHeader "requesting-user"
incrementCounter (Just uid) = do
  i <- counter
  logInfo "incrementCounter" $ uid ++ " incrementing counter"
  _ <- liftRedis $ R.set "counter" $ toStrict $ AE.encode $ i+1
  return NoContent

--
-- | Reset the counter
resetCounter :: (MonadLogger m, MonadError AppError m, MonadRedis m) =>
  Maybe UUID -> -- ^ requesting-user header
  m NoContent
resetCounter Nothing = throwError $ MissingHeader "requesting-user"
resetCounter (Just uid) = do
  logInfo "resetCounter" $ uid ++ " resetting counter"
  _ <- liftRedis $ R.set "counter" $ toStrict $ AE.encode $ (0 :: Int)
  return NoContent

--
-- Widget handlers
--

--
-- | List widgets
listWidgets :: (MonadLogger m, MonadError AppError m, MonadIO m) =>
  PersonId ->
  ReaderT SqlBackend m (Document Widget)
listWidgets pid = do
  logInfo "listWidgets" $ "Listing widgets for " ++ show pid
  res <- E.select $ E.from $ \(w `E.InnerJoin` pw) -> do
    E.on $ pw E.^. PersonWidgetWid E.==. w E.^. WidgetId
    E.where_ $ pw E.^. PersonWidgetPid E.==. E.val pid
    return w
  return $ mkDocument (map entityVal res) Nothing Nothing

--
-- | Add a widget for a user
addWidget :: (MonadIO m, MonadLogger m, MonadError AppError m) =>
  PersonId ->
  Maybe UUID -> -- ^ requesting-user
  Widget ->
  ReaderT SqlBackend m NoContent
addWidget _ Nothing _      = throwError (MissingHeader "requesting-user")
addWidget pid (Just uid) w = do
  logInfo "addWidget" $ uid ++ " adding widget " ++ show w ++ " for user " ++ show (fromIntegral pid :: Int)
  -- Here my ignorance of SQL shows
  res <- E.select $ E.from $ \w' -> do
    E.where_ $ w' E.^. WidgetName E.==. E.val (widgetName w)
    E.where_ $ w' E.^. WidgetColor E.==. E.val (widgetColor w)
    return $ w' E.^. WidgetId
  k <- if length res == 0
          then do
            logInfo "addWidget" $ "Adding widget " ++ show w
            insert w
          else return $ E.unValue $ head $ res
  _ <- insert $ PersonWidget pid k
  return NoContent

--
-- | Delete a widget from a user's list
deleteWidget :: (MonadIO m, MonadLogger m, MonadError AppError m) =>
  PersonId ->
  WidgetId ->
  Maybe UUID -> -- ^ requesting-user
  ReaderT SqlBackend m NoContent
deleteWidget _ _ Nothing = throwError $ MissingHeader "requesting-user"
deleteWidget pid wid (Just uid) =  do
  logInfo "deleteWidget" $ uid ++ " deleting widget (" ++ show pid ++ ", " ++ show wid ++ ")"
  E.delete $ E.from $ \pw -> do
    E.where_ $ pw E.^. PersonWidgetPid E.==. E.val pid
    E.where_ $ pw E.^. PersonWidgetWid E.==. E.val wid
  return NoContent

--
-- | Retrieve the users with a certain widget
usersWith :: (MonadIO m, MonadLogger m, MonadError AppError m) =>
  Types.Text -> -- ^ widget name
  ReaderT SqlBackend m (Document Person)
usersWith name = do
  logInfo "usersWith" $ "Listing widgets with name " ++ show name
  res <- E.select $ E.from $ \(p `E.InnerJoin` pw `E.InnerJoin` w) -> do
    E.on (pw E.^. PersonWidgetWid E.==. w E.^. WidgetId)
    E.on (pw E.^. PersonWidgetPid E.==. p E.^. PersonId)
    E.where_ (w E.^. WidgetName E.==. E.val name)
    return p
  return $ mkDocument (map entityVal res) Nothing Nothing

-- | Get the keys for a widget with a certain description
getWidgetId :: (MonadError AppError m, MonadLogger m, MonadIO m) =>
  String ->
  String ->
  ReaderT SqlBackend m [WidgetId]
getWidgetId name color = do
  logInfo "getWidgetId" $ "Getting the id of name = " ++ show name ++ " and color = " ++ show color
  res <- selectList [WidgetName ==. name, WidgetColor ==. color] []
  return $ fmap entityKey res

--
-- Logging utilities

logInfo :: MonadLogger m => Text -> String -> m ()
logInfo src msg = monadLoggerLog defaultLoc src LevelInfo msg

logError :: MonadLogger m => Text -> String -> m ()
logError src msg = monadLoggerLog defaultLoc src LevelError msg
