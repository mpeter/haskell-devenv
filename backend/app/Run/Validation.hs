-- |
-- Module Run.Validation
--
-- Helper functions to validate values
module Run.Validation where

import           Text.Regex.TDFA ((=~))

-- | A valid name query param value should be 'last, first
parseName :: String -> Either String (String, String)
parseName name
  | name =~ re = case words $ filter (/=',') name of
    [l,f] -> Right (f, l)
    _     -> Left err
  | otherwise = Left err
  where
    re = "^[a-zA-Z]{1,64}, [a-zA-Z]{1,64}$" :: String
    err = "Names should be formatted 'last, first'"

-- | A valid SSN should be 'xxx-xx-xxxx'
validateSsn :: String -> Bool
validateSsn = flip (=~) re
  where
    re = "^[0-9]{3}-[0-9]{2}-[0-9]{4}$" :: String
