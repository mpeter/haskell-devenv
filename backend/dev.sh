#!/bin/sh
set -e

echo "Building...."
stack build

echo "Starting redis..."
redis-server &
REDIS_PID=$!

echo "Creating development database..."
export PGCONN="$(pg_tmp -w 3600 -d "$(pwd)/devdb" start)"
echo "Database URL: " "$PGCONN"

trap "echo 'Stopping...; kill $REDIS_PID; sleep 1; pg_tmp -d $(pwd)/devdb stop" INT

set +e
echo "Running 'stack exec example-server'..."
stack exec example-server -- "$PGCONN"
RESULT=$?

echo "Stopping development database..."
pg_tmp  -d "$(pwd)/devdb" stop

echo "Stopping redis..."
kill $REDIS_PID

[[ "$RESULT" == "0" ]] && exit 0 || exit 1
