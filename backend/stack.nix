# nix environment for stack's nix integration
# see https://docs.haskellstack.org/en/stable/nix_integration/
let
  pkgs = import ../pinned-nixpkgs.nix {};
in { ghc, ... } : pkgs.haskell.lib.buildStackProject {
  ghc = ghc;
  name = "haskell-elm-app-backend-env";
  buildInputs = [ pkgs.zlib pkgs.gmp pkgs.git pkgs.postgresql95 ];
  enableSharedExecutables = false;
  configureFlags = [ "--ghc-option=-optl=-static" "--ghc-option=-optl=-pthread" ];
}
