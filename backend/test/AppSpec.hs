{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeOperators             #-}

module AppSpec (spec) where

import           Api
import           Control.Lens                     (view)
import           Control.Monad.IO.Class           (liftIO)
import           Control.Monad.Logger             (LoggingT, runStdoutLoggingT)
import           Control.Monad.Reader             (ReaderT (..))
import           Control.Monad.Trans              (lift)
import           Control.Monad.Trans.Maybe        (MaybeT (..))
import           Data.ByteString.Lazy             (ByteString)
import           Data.Either                      (isLeft, isRight)
import           Data.Proxy
import           Data.Swagger.Schema.Validation   (validateToJSON)
import           Data.Time                        (defaultTimeLocale,
                                                   getCurrentTime,
                                                   parseTimeOrError)
import           Database.Persist                 (Filter, deleteWhere)
import           Database.Persist.Postgresql      (withPostgresqlConn)
import           Database.Persist.Sqlite          (withSqliteConn)
import           Network.HTTP.Client              (Manager,
                                                   defaultManagerSettings,
                                                   newManager)
import           Network.JSONApi                  (Document, ResourceData (..),
                                                   dataL, getResource)
import           Network.Wai.Handler.Warp         (testWithApplication)
import           Run.Db                           (RunDb, initDb, connString)
import           Run.Servers                      (mkApp)
import           Servant.API                      ((:<|>) (..), NoContent)
import           Servant.API.ContentTypes.JSONApi ()
import           Servant.Client
import           System.Environment               (setEnv)
import           Test.Hspec
import           Test.Mockery.Directory           (inTempDirectory)
import           Types

--
-- Generate an API client with Servant.Client
--

-- We state the type sigantures for the functions that will interact with the
-- API, then Servant will generate implementations of them automatically
createPerson :: Maybe UUID -> CreateReq -> ClientM (Document Person)
editPerson :: Maybe UUID -> UpdateReq -> ClientM NoContent
getPersonById :: PersonId -> ClientM (Document Person)
getPerson :: Maybe Text -> Maybe Text -> ClientM (Document Person)
deletePersonById :: PersonId -> ClientM NoContent
getCreatedAtById :: PersonId -> ClientM DateTime
getUpdatedAtById :: PersonId -> ClientM DateTime
getPersonId :: UUID -> ClientM [PersonId]

counter :: ClientM Int
incrementCounter :: Maybe UUID -> ClientM NoContent
resetCounter :: Maybe UUID -> ClientM NoContent

listWidgets :: PersonId -> ClientM (Document Widget)
addWidget :: PersonId -> Maybe UUID -> Widget -> ClientM NoContent
deleteWidget :: PersonId -> WidgetId -> Maybe UUID -> ClientM NoContent
usersWith :: Text -> ClientM (Document Person)
getWidgetId :: Text -> Text -> ClientM [WidgetId]

-- Populate the types we defined above with values
createPerson
  :<|> editPerson
  :<|> deletePersonById
  :<|> getPersonById
  :<|> getPerson
  :<|> getCreatedAtById
  :<|> getUpdatedAtById
  :<|> getPersonId = client (Proxy :: Proxy PersonRecordAPI)

counter
  :<|> incrementCounter
  :<|> resetCounter = client (Proxy :: Proxy CounterAPI)

listWidgets
  :<|> addWidget
  :<|> deleteWidget
  :<|> usersWith
  :<|> getWidgetId = client (Proxy :: Proxy WidgetAPI)

--
-- Example data
--

grantDob :: Date
grantDob = parseTimeOrError True defaultTimeLocale "%Y-%m-%d" "1822-04-27"

grantDod :: Date
grantDod = parseTimeOrError True defaultTimeLocale "%Y-%m-%d" "1885-07-23"

cRequest :: CreateReq
cRequest = CR "Ulysses" "S" "Grant" "" "555-00-1234" grantDob False Nothing

uRequest :: UUID -> UpdateReq
uRequest uid = UR uid "Ulysses" "S" "Grant" "" "555-00-1234" grantDob True (Just grantDod)


--
-- Helper functions
--

serverSession :: forall a. (Int -> IO a) -> IO a
serverSession action = do
  cs <- connString
  print "X"
  inTempDirectory $ do
    -- initDb psqlConnStrTest
    -- let app = mkApp psqlConnStrTest
    -- ret <- testWithApplication (return app) action
    -- -- Clear the test data
    -- print "(Weakly) deleting test data"
    -- let
    --   del :: ReaderT SqlBackend (LoggingT IO) ()
    --   del = do
    --     deleteWhere ([] :: [Filter PersonWidget])
    --     deleteWhere ([] :: [Filter Person])
    --     deleteWhere ([] :: [Filter Widget])
    -- runStdoutLoggingT $
    --   withPostgresqlConn psqlConnStrTest $
    --     runReaderT $ del
    -- return ret

    runStdoutLoggingT $ withPostgresqlConn cs $ \x -> do
      let x' = flip runReaderT x
      initDb x'
      liftIO $ testWithApplication (return $ mkApp x') action

    -- runStdoutLoggingT $ withSqliteConn ":memory:" $ \x -> do
    --   let x' = flip runReaderT x
    --   initDb x'
    --   liftIO $ testWithApplication (return $ mkApp x') action

defaultClientEnv :: Manager -> Int -> ClientEnv
defaultClientEnv mgr port = ClientEnv mgr (BaseUrl Http "localhost" port "")

runC :: Manager -> Int -> ClientM a -> IO (Either ServantError a)
runC mgr port = flip runClientM $ defaultClientEnv mgr port

testErr :: Show a => ClientM a -> Int -> IO ()
testErr action port = do
  mgr <- newManager defaultManagerSettings
  res <- runC mgr port action
  res `shouldSatisfy` isLeft

docHead :: Monad m => Document a -> MaybeT m a
docHead doc = MaybeT $ return $ case view dataL doc of
  Singleton x -> Just $ getResource x
  List (x:xs) -> Just $ getResource x
  _           -> Nothing

--
-- Tests
--

spec :: Spec
spec = do
  around serverSession $ do
    describe "JSON instances" $ do
      it "Validates Person" $ \_ -> do
        now <- getCurrentTime
        let
          testP =
            Person
              "abc"
              "Grant"
              "S"
              "Ulysses"
              ""
              "555-00-1234"
              grantDob
              True
              (Just grantDod)
              now
              Nothing
              "user123"
              Nothing
        validateToJSON testP `shouldBe` []

      let testW = Widget "00FFAA" "test-widget"
      it "Validates Widget" $ \_ -> do
        validateToJSON testW `shouldBe` []

    describe "Person API" $ do

      -- Request a nonexistent person
      it "Responds with an error for nonexistent people" $ testErr (getPersonById 1500)

      -- Update a nonexistent person
      it "Responds with an error for nonexistent people" $
        testErr (editPerson (Just "UUID-7") $ uRequest "garbage")

      it "Responds with the full person record" $ \port -> do
        mgr <- newManager defaultManagerSettings
        res <- flip runClientM (defaultClientEnv mgr port) $ do
          createPerson (Just "UUID-8") cRequest
        res `shouldSatisfy` isRight

      -- Create a person
      -- Update the person
      it "Creates a person, responding with that person, then edits the person" $ \port -> do
        mgr <- newManager defaultManagerSettings
        res <- flip runClientM (defaultClientEnv mgr port) $ do
          grant <- createPerson (Just "UUID-7") cRequest
          runMaybeT $ do
            uid <- fmap (view uniqueIdL) $ docHead grant
            lift $ editPerson (Just "UUID-5") $ uRequest uid
        res `shouldSatisfy` isRight

    -- Increment counter
    -- Reset counter
    describe "GET /counter; GET /counter/increment; GET /counter/reset" $ do
      it "Checks that the counter actually increments and resets" $ \port -> do
        mgr <- newManager defaultManagerSettings
        let run = flip runClientM (defaultClientEnv mgr port)
            uid = Just "1234"
            action = run $ do
              resetCounter uid
              i0 <- counter
              incrementCounter uid
              i1 <- counter
              resetCounter uid
              i2 <- counter
              incrementCounter uid
              incrementCounter uid
              i3 <- counter
              return [i0,i1,i2,i3]
        action `shouldReturn` Right [0,1,0,2]

    describe "Widget endpoints" $ do
      it "Creates a user and adds a widget" $ \port -> do
        mgr <- newManager defaultManagerSettings
        let
          w = Widget "00AAAA" "calculator"
          action = flip runClientM (defaultClientEnv mgr port) $ do
            p0 <- createPerson (Just "UUID-1") cRequest
            runMaybeT $ do
              p <- docHead p0
              let uid = view uniqueIdL p
              k <- lift $ head <$> getPersonId uid
              lift $ addWidget k (Just "UUID-2") w
              ws <- lift $ listWidgets k
              p' <- lift (usersWith "calculator") >>= docHead
              let
                member x y = case view dataL y of
                  Singleton x' -> x == (getResource x')
                  List xs'     -> elem x (fmap getResource xs')
                crit2 = view uniqueIdL p' == view uniqueIdL p
              wid <- fmap head $ lift $ getWidgetId "calculator" "00AAAA"
              lift $ deleteWidget k wid $ Just "UUID-7"
              ws2 <- lift $ listWidgets k
              let crit3 = member w ws2
              return $ (member w ws, crit2, crit3)
        action `shouldReturn` Right (Just (True, True, False))
